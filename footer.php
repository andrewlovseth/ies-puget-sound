	
	<footer>
		<div class="wrapper">
					
			<section class="separator">
				<div class="wrapper">

					<?php get_template_part('partials/illumination-white'); ?>

				</div>
			</section>


			<section class="footer-content">

				<div class="logo-nav">

					<div class="logo">
						<a href="<?php echo site_url('/'); ?>">
							<?php $classes = get_body_class(); if (in_array('page-tribe-attendee-registration',$classes)): ?>
								<?php $imageID = get_field('footer_logo', 'options'); echo wp_get_attachment_image( $imageID, 'full' ); ?>
							<?php else: ?>
								<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>
							<span><?php the_field('chapter_tag', 'options'); ?></span>
						</a>
					</div>

					<nav>

						<a href="<?php echo site_url('/'); ?>" class="home">Home</a>

						<?php $posts = get_field('main_nav', 'options'); if( $posts ): ?>

							<div class="row">
   								<a href="<?php echo site_url('/events/'); ?>">Events</a>

							    <?php foreach( $posts as $post): setup_postdata($post); ?>

							    	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

							    <?php endforeach; ?>

							</div>

						<?php wp_reset_postdata(); endif; ?>

						<?php $posts = get_field('utility_nav', 'options'); if( $posts ): ?>
						   	
						   	<div class="row">

							    <?php foreach( $posts as $post): setup_postdata($post); ?>

							    	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

							    <?php endforeach; ?>

							</div>

						<?php wp_reset_postdata(); endif; ?>


					</nav>

				</div>

				<div class="social-copyright">

					<div class="social-links">
						<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>
						 
						    <a href="<?php the_sub_field('link'); ?>" rel="external">
								<?php $classes = get_body_class(); if (in_array('page-tribe-attendee-registration',$classes)): ?>
									<?php $imageID = get_sub_field('icon'); echo wp_get_attachment_image( $imageID, 'full' ); ?>
								<?php else: ?>
									<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php endif; ?>
						    </a>

						<?php endwhile; endif; ?>
					</div>

					<div class="copyright">
						<?php the_field('copyright', 'options'); ?>
					</div>


				</div>


			</section>



		</div>
	</footer>

	



	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>