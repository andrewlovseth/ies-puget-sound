$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('nav#mobile').slideToggle(300);



		return false;

	});


	$('.art-body').slick({
		dots: false,
		infinite: true,
		speed: 300,
		centerMode: true,
		variableWidth: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
		responsive: [
			{
			  breakpoint: 480,
			  settings: {
			    slidesToShow: 2,
			    slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 2
			  }
			},
		]
	});


	// Show Zoom In
	$('#image .expand, .image .expand').click(function(){

		var modal = $(this).parent('.thumbnail').siblings('.modal');
		$(modal).show();

		return false;
	});
    

	// Hide Zoom In
	$('#image .close, .image .close').click(function(){
		
		var modal = $(this).parent('.modal');
		$(modal).hide();

		return false;
	});
    



});


$(window).load(function() {

	var $grid = $('.gallery').imagesLoaded( function() {
		$grid.masonry({
			itemSelector: '.asset',
			columnWidth: '.grid-sizer',
			gutter: '.gutter-sizer',
			percentPosition: true,
			animate: true,
			stagger: 30
		});
	});

});