<div class="logo">
	<a href="<?php echo site_url('/'); ?>">
		<?php $classes = get_body_class(); if (in_array('page-tribe-attendee-registration',$classes)): ?>
			<?php $imageID = get_field('logo_desktop', 'options'); echo wp_get_attachment_image( $imageID, 'full' ); ?>
		<?php else: ?>
			<img src="<?php $image = get_field('logo_desktop', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endif; ?>

		<span><?php the_field('chapter_tag', 'options'); ?></span>
	</a>
</div>
