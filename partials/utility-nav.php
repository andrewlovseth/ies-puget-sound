<?php $utility_nav_links = get_field('utility_nav', 'options'); if( $utility_nav_links ): ?>
   	
   	<nav id="utility">
   		<div class="wrapper">
	
		    <?php foreach( $utility_nav_links as $utility_nav_link): ?>
		    	<a href="<?php echo get_permalink( $utility_nav_link ); ?>"><?php echo get_the_title( $utility_nav_link ); ?></a>
		    <?php endforeach; ?>

		</div>
	</nav>

<?php wp_reset_postdata(); endif; ?>