<div class="person">
	<div class="name">
		<?php if(get_sub_field('title')): ?>
			<h4><?php the_sub_field('title'); ?></h4>
		<?php endif; ?>
		<h5><?php the_sub_field('name'); ?></h5>
	</div>

	<div class="email">
		<a href="mailto:<?php the_sub_field('email'); ?>">Email</a>
	</div>

	<div class="phone">
		<span><?php the_sub_field('phone'); ?></span>
	</div>
</div>