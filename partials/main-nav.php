<?php $main_nav_links = get_field('main_nav', 'options'); if( $main_nav_links ): ?>
   	
   	<nav id="main">
   		<a href="<?php echo site_url('/events/'); ?>">Events</a>

	    <?php foreach( $main_nav_links as $main_nav_link): ?>
		    <a href="<?php echo get_permalink( $main_nav_link ); ?>"><?php echo get_the_title( $main_nav_link ); ?></a>
	    <?php endforeach; ?>
	</nav>

<?php wp_reset_postdata(); endif; ?>