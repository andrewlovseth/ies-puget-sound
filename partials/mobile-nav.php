<nav id="mobile">
	<div class="wrapper">

		<a href="<?php echo site_url('/'); ?>">Home</a>
   		<a href="<?php echo site_url('/events/'); ?>">Events</a>

		<?php $main_links = get_field('main_nav', 'options'); if( $main_links ): ?>

		    <?php foreach( $main_links as $main_link): ?>

		    	<a href="<?php echo get_permalink( $main_link->ID ); ?>"><?php echo get_the_title( $main_link->ID ); ?></a>

		    <?php endforeach; ?>

		<?php wp_reset_postdata(); endif; ?>

		<?php $utility_links = get_field('utility_nav', 'options'); if( $utility_links ): ?>
		   	
		    <?php foreach( $utility_links as $utility_link): ?>

		    	<a href="<?php echo get_permalink( $utility_link->ID ); ?>"><?php echo get_the_title( $utility_link->ID ); ?></a>

		    <?php endforeach; ?>

		<?php wp_reset_postdata(); endif; ?>

	</div>
</nav>