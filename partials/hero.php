<section id="hero" class="cover inner" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
	<div class="wrapper">

		<div class="info">
			<h1><?php the_title(); ?></h1>
		</div>

	</div>
</section>