<?php

/*

	Template Name: Resources

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<div class="description">
				<?php the_field('content'); ?>
			</div>

			<section id="resources-list">
				
				<?php if(have_rows('resource_categories')): while(have_rows('resource_categories')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'category' ): ?>

				    	<div class="resource-category">

				    		<h3><?php the_sub_field('category_header'); ?></h3>

						    <?php if(have_rows('resources')): while(have_rows('resources')): the_row(); ?>
						 
							    <div class="resource">
							        <h4><a href="<?php the_sub_field('link'); ?>" rel="external"><?php the_sub_field('title'); ?></a></h4>
							        <?php if(get_sub_field('description')): ?>
								        <?php the_sub_field('description'); ?>
								    <?php endif; ?>
							    </div>

							<?php endwhile; endif; ?>

							<?php get_template_part('partials/illumination-blue'); ?>

						</div>
								
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>

			</section>

		</div>
	</section>


<?php get_footer(); ?>