<?php

/*

	Template Name: Membership

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main" class="two-col">
		<div class="wrapper">	

			<article>
				<?php the_field('content'); ?>
			</article>

			<aside>

				<div class="how-to">
					<h3>How to Become a Member</h3>

					<ol>
						<?php if(have_rows('how_to')): while(have_rows('how_to')): the_row(); ?>
						 
						 	<li><?php the_sub_field('step'); ?></li>

						<?php endwhile; endif; ?>
					</ol>

					<a href="<?php the_field('join_link'); ?>" class="btn" rel="external">Join</a>
				</div>

			</aside>

		</div>
	</section>


<?php get_footer(); ?>