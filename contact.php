<?php

/*

	Template Name: Contact

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main" class="two-col">
		<div class="wrapper">	

			<article>
				<?php the_field('content'); ?>

				<section id="section-officers" class="list">
					<h3>Section Officers</h3>
					
					<?php if(have_rows('officers')): while(have_rows('officers')): the_row(); ?>
					 
						<?php get_template_part('partials/contact-row'); ?>

					<?php endwhile; endif; ?>
				</section>


				<section id="board-of-manageres" class="list">
					<h3>Board of Managers</h3>
					
					<?php if(have_rows('board')): while(have_rows('board')): the_row(); ?>
					 
						<?php get_template_part('partials/contact-row'); ?>

					<?php endwhile; endif; ?>
				</section>


				<section id="committee-chars" class="list">
					<h3>Committee Chairs</h3>
					
					<?php if(have_rows('chairs')): while(have_rows('chairs')): the_row(); ?>
					 
						<?php get_template_part('partials/contact-row'); ?>

					<?php endwhile; endif; ?>
				</section>

			</article>

			<aside>

				<h3>Get in touch</h3>

				<?php echo do_shortcode('[wpforms id="372"]'); ?>

			</aside>

		</div>
	</section>


<?php get_footer(); ?>