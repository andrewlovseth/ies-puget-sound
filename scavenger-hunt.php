<?php

/*

	Template Name: Scavenger Hunt

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main" class="two-col">
		<div class="wrapper">	

			<article>

				<?php the_field('content'); ?>


				<section id="gallery">
					<h3><?php the_field('gallery_headline'); ?></h3>
					<?php the_field('gallery'); ?>
				</section>

			</article>

			<aside>
				<h3><?php the_field('sidebar_headline'); ?></h3>
				<?php the_field('sidebar_content'); ?>
				<a href="<?php the_field('cta_link'); ?>" class="btn"><?php the_field('cta_label'); ?></a>
			</aside>

		</div>
	</section>


<?php get_footer(); ?>