<?php

/*

	Template Name: Education

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main" class="two-col">
		<div class="wrapper">	

			<article>

				<section id="fundamentals" class="course">
					<h3>Fundamentals of Lighting</h3>
					<?php the_field('fundamentals_content'); ?>

					<div class="schedule">

						<?php if(have_rows('fundamentals_schedule')): ?>

							<div class="header">
								<span class="course">Course</span>
								<span class="date">Date</span>
								<span class="curriculum">Curriculum</span>
								<span class="instructor">Instructor</span>
							</div>

							<?php while(have_rows('fundamentals_schedule')): the_row(); ?>
						 
							    <div class="class">
							        <span class="course"><?php the_sub_field('module'); ?></span>
							        <span class="date"><?php the_sub_field('date'); ?></span>
							        <span class="curriculum"><?php the_sub_field('curriculum'); ?></span>
							        <span class="instructor"><?php the_sub_field('instructor'); ?></span>
							    </div>

							<?php endwhile; ?>
							
						<?php endif; ?>			

					</div>


					<?php if(get_field('fundamentals_registration_open')): ?>
						<a href="<?php the_field('fundamentals_link'); ?>" class="btn">Register Now</a>
					<?php else: ?>
						<p class="closed">Registration is currently closed. Check back soon for dates and registration options.</p>
					<?php endif; ?>


				</section>


				<section id="intermediate" class="course">
					<h3>Intermediate Lighting Seminars</h3>
					<?php the_field('intermediate_content'); ?>

					<div class="schedule">

						<?php if(have_rows('intermediate_schedule')): ?>

							<div class="header">
								<span class="course">Course</span>
								<span class="date">Date</span>
								<span class="curriculum">Curriculum</span>
								<span class="instructor">Instructor</span>
							</div>

							<?php while(have_rows('intermediate_schedule')): the_row(); ?>
						 
							    <div class="class">
							        <span class="course"><?php the_sub_field('module'); ?></span>
							        <span class="date"><?php the_sub_field('date'); ?></span>
							        <span class="curriculum"><?php the_sub_field('curriculum'); ?></span>
							        <span class="instructor"><?php the_sub_field('instructor'); ?></span>
							    </div>

							<?php endwhile; ?>
							
						<?php endif; ?>						

					</div>

					<?php if(get_field('intermediate_registration_open')): ?>
						<a href="<?php the_field('intermediate_link'); ?>" class="btn">Register Now</a>
					<?php else: ?>
						<p class="closed">Registration is currently closed. Check back soon for dates and registration options.</p>
					<?php endif; ?>

				</section>


			</article>

			<aside>

				<div class="about">
					<?php the_field('sidebar_content'); ?>
				</div>

			</aside>

		</div>
	</section>


<?php get_footer(); ?>