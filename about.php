<?php

/*

	Template Name: About

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main" class="two-col">
		<div class="wrapper">	

			<article>
				<?php the_field('content'); ?>
			</article>

			<aside>
				<h3>Section Officers</h3>
				
				<?php if(have_rows('officers')): while(have_rows('officers')): the_row(); ?>
				 
				    <div class="officer">
				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

				    	<div class="info">
					        <h4><?php the_sub_field('title'); ?></h4>
					        <h5><?php the_sub_field('name'); ?></h5>
					        <a href="mailto:<?php the_sub_field('email'); ?>">Email</a>
					    </div>
				    </div>

				<?php endwhile; endif; ?>
			</aside>

		</div>
	</section>


<?php get_footer(); ?>