<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

?>

<section id="event-single">
	<div class="wrapper">



		<article>

			<div class="header">
				<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>
			</div>

			<div class="meta">

				<div class="date">
					<span class="month"><?php echo tribe_get_start_date( null, true, 'M' ); ?></span>
					<span class="day"><?php echo tribe_get_start_date( null, true, 'j' ); ?></span>
				</div>

				<div class="info">
					<h5 class="time"><?php echo tribe_get_start_date( null, true, 'g:ia' ); ?></h5>
					<h5 class="location"><?php echo tribe_get_venue(); ?></h5>
					<h5 class="cost"><?php echo tribe_get_formatted_cost(); ?></h5>
				</div>

			</div>

			<div class="description">
				<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
				<?php the_content(); ?>
			</div>

			<div class="cal">
				<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
			</div>

			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
			<?php tribe_get_template_part( 'modules/meta' ); ?>
			<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>

		</article>
		
		<aside>

			<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		</aside>


	</div>
</section>
