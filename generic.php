<?php

/*

	Template Name: Generic

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main">

		<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
		 
		    <?php if( get_row_layout() == 'wysiwyg' ): ?>
				
				<section class="wysiwyg">
					<div class="wrapper">
			    		<?php the_sub_field('content'); ?>
			    	</div>
				</section>
				
		    <?php endif; ?>


		    <?php if( get_row_layout() == 'full_width_image' ): ?>
				
				<section class="full-width-image">
					<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</section>
				
		    <?php endif; ?>


		    <?php if( get_row_layout() == 'three_col_image' ): ?>
				
				<section class="three-col-image">
					<div class="wrapper">
						<?php if(have_rows('images')): while(have_rows('images')): the_row(); ?>
						 
						    <div class="col">
						    	<div class="image">
						    		<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    	</div>

						    	<div class="info">
							        <?php the_sub_field('caption'); ?>
							    </div>
						    </div>

						<?php endwhile; endif; ?>
						</div>
				</section>
				
		    <?php endif; ?>


		    <?php if( get_row_layout() == 'call_to_action' ): ?>
				
				<section class="call-to-action">
					<a href="<?php the_sub_field('link'); ?>" class="btn"><?php the_sub_field('link_label'); ?></a>
				</section>
				
		    <?php endif; ?>
		 
		<?php endwhile; endif; ?>

	</section>

<?php get_footer(); ?>