<?php

/*

	Template Name: Home

*/

get_header(); ?>



	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h1><?php the_field('hero_headline'); ?></h1>

				<?php get_template_part('partials/illumination-white'); ?>
				
				<h2><?php the_field('hero_sub_headline'); ?></h2>
			</div>

		</div>
	</section>


	<section id="about-statement">
		<div class="wrapper">

			<?php the_field('about'); ?>

		</div>
	</section>


	<section class="separator">
		<div class="wrapper">

			<?php get_template_part('partials/illumination-blue'); ?>

		</div>
	</section>


	<section id="upcoming">
		<div class="wrapper">


			<section id="upcoming-events">
				<h3>Upcoming Events</h3>


				<?php
					$events = tribe_get_events( array(
						'posts_per_page' => 3,
						'eventDisplay' => 'list'
					), true );

					if( $events->have_posts() ) : while( $events->have_posts() ) : $events->the_post(); ?>
						
						<div class="event">

							<div class="date">
								<span class="month"><?php echo tribe_get_start_date( null, true, 'M' ); ?></span>
								<span class="day"><?php echo tribe_get_start_date( null, true, 'j' ); ?></span>
							</div>

							<div class="info">
								<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								<h5 class="time"><?php echo tribe_get_start_date( null, true, 'g:ia' ); ?></h5>
								<h5 class="location"><?php echo tribe_get_venue(); ?></h5>
								<h5 class="cost"><?php echo tribe_get_formatted_cost(); ?></h5>
							</div>
							
						</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>


				<div class="btn-wrapper">
					<a href="<?php echo site_url('/events/'); ?>" class="btn fancy"><span>See all events</span></a>
				</div>

			</section>



			<section id="upcoming-classes">
				<h3>Upcoming Classes</h3>


				<?php if(have_rows('classes')): while(have_rows('classes')): the_row(); ?>
				 
					<div class="event">

						<div class="date">
							<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="info">
							<h4><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('name'); ?></a></h4>
							<h5 class="time"><?php the_sub_field('date'); ?></h5>
							<h5 class="location"><?php the_sub_field('location'); ?></h5>
							<h5 class="cost"><?php the_sub_field('price'); ?></h5>

							<a href="<?php the_sub_field('link'); ?>" class="btn fancy"><span>Learn more</span></a>
						</div>
						
					</div>

				<?php endwhile; endif; ?>

			</section>

		</div>
	</section>


	<section id="member-spotlight">
		<div class="wrapper">

			<section class="header">
				<h3>Member Spotlight</h3>
				<h4><?php the_field('season'); ?></h4>
			</section>

			<section class="feature">
				<div class="photo">
					<img src="<?php $image = get_field('member_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="info">
					<h5><?php the_field('member_name'); ?></h5>
					<?php the_field('member_bio'); ?>
				</div>
			</section>

			<section id="member-gallery">
				<?php the_field('member_gallery_wysiwyg'); ?>
			</section>

		</div>
	</section>


<?php get_footer(); ?>