<?php

/*

	Template Name: Scholarship

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main" class="two-col">
		<div class="wrapper">	

			<article>
				<?php the_field('content'); ?>
			</article>

			<aside>
				<div class="apply">
					<h3>How to Apply</h3>
					<?php the_field('how_to_apply'); ?>
					<a href="<?php the_field('apply_link'); ?>" rel="external" class="btn">Apply Now</a>
				</div>
			</aside>

		</div>
	</section>


<?php get_footer(); ?>