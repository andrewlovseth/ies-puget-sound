<?php get_header(); ?>

    <section id="woocommerce">
		<div class="wrapper">

			<?php woocommerce_content(); ?>

	    </div>
    </section>

<?php get_footer(); ?>