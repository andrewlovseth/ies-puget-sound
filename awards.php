<?php

/*

	Template Name: Awards

*/

get_header(); ?>


	<?php get_template_part('partials/hero'); ?>

	<section id="main" class="two-col">
		<div class="wrapper">	

			<article>

				<?php the_field('content'); ?>

				<section id="award-list">

					<?php if(have_rows('awards')): while(have_rows('awards')): the_row(); ?>
					 
					    <div class="award">

					    	<div class="header">

					    		<div class="title">
						    		<h4><?php the_sub_field('title'); ?></h4>
						    		<?php if(get_sub_field('sponsor')): ?>
							    		<em><?php the_sub_field('sponsor'); ?></em>
							    	<?php endif; ?>
						    	</div>

						    	<a href="<?php the_sub_field('score_sheet'); ?>" class="btn score-sheet">Score Sheet</a>
						    	
						    	<a href="<?php the_sub_field('apply_link'); ?>" rel="external" class="btn apply">Apply</a>

					    	</div>

					        <?php the_sub_field('decsription'); ?>

					    </div>

					<?php endwhile; endif; ?>

				</section>


				<section id="gallery">
					<h3><?php the_field('gallery_headline'); ?></h3>
					<?php the_field('gallery'); ?>
				</section>


				<section id="dinner">
					<h3><?php the_field('dinner_headline'); ?></h3>
					<?php the_field('dinner_content'); ?>
				</section>

			</article>


		</div>
	</section>


<?php get_footer(); ?>