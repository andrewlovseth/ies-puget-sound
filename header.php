<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width" />

	<link href="//fonts.googleapis.com/css?family=Crimson+Text:400,400i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php get_template_part('partials/utility-nav'); ?>

	<header class="site-header">
		<div class="wrapper">

			<?php get_template_part('partials/header-logo'); ?>

			<?php get_template_part('partials/main-nav'); ?>

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>

		</div>
	</header>

	<?php get_template_part('partials/mobile-nav'); ?>


